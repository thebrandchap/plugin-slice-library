<?php
/*
Plugin Name:			Slice Library
Description: 			Creates a custom post type for creating libraries of slices to be included elsewhere in the site. Requires Sitebooster Theme and ACF.
Version:           		1.0.0
Author:            		Glyn Harrison
License:           		GNU General Public License v2
License URI:       		http://www.gnu.org/licenses/gpl-2.0.html
Plugin URI: https://bitbucket.org/thebrandchap/plugin-slice-library/
Bitbucket Plugin URI: https://bitbucket.org/thebrandchap/plugin-slice-library/
*/
function Slice() {

	$labels = array(
		'name'                  =>	'Slice Library',
		'singular_name'         =>	'Slices',
		'menu_name'             =>	'Slice Library',
		'name_admin_bar'        =>	'Slice Library',
		'archives'              =>	'',
		'attributes'            =>	'',
		'parent_item_colon'     =>	'',
		'all_items'             =>	'All slices',
		'add_new_item'          =>	'Add new slice',
		'add_new'               =>	'Add new slice',
		'new_item'              =>	'New slice',
		'edit_item'             =>	'Edit slice',
		'update_item'           =>	'Update slice',
		'view_item'             =>	'View slice',
		'view_items'            =>	'View slice',
		'search_items'          =>	'Search slice library',
		'not_found'             =>	'Not found',
		'not_found_in_trash'    =>	'Not found in trash',
		'insert_into_item'      =>	'Insert into slice library',
		'uploaded_to_this_item' =>	'Uploaded to this slice library',
		'items_list'            =>	'Slice list',
		'items_list_navigation' =>	'Slice list navigation',
		'filter_items_list'     =>	'Filter Library',
	);
	$args = array(
		'label'                 =>	'Slice',
		'description'           =>	'Slices',
		'labels'                =>	$labels,
		'supports' 				=>	array('title', 'editor'),
		'hierarchical'          =>	false,
		'public'                =>	true,
		'show_ui'               =>	true,
		'show_in_menu'          =>	true,
		'menu_position'         =>	5,
		'menu_icon'             =>	'dashicons-align-center',
		'show_in_admin_bar'     =>	true,
		'show_in_nav_menus'     =>	true,
		'can_export'            =>	true,
		'has_archive'           =>	false,
		'exclude_from_search'   =>	true,
		'publicly_queryable'    =>	false,
		'capability_type'       =>	'page',
		'supports' 				=>	array('title', 'page-attributes'),
	);
	register_post_type( 'slices', $args );

}
add_action( 'init', 'Slice', 0 );



if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5ab289aba8bcd',
	'title' => 'Slice - Add from Library',
	'fields' => array(
		array(
			'key' => 'field_5ab289cb37e19',
			'label' => 'Pick a Library',
			'name' => 'library',
			'type' => 'relationship',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array(
				0 => 'slices',
			),
			'taxonomy' => array(
			),
			'filters' => array(
				0 => 'search',
			),
			'elements' => '',
			'min' => '',
			'max' => '',
			'return_format' => 'id',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 0,
	'description' => '',
));

endif;

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5ab289aba8bcd',
	'title' => 'Library - Slice - Add from Library',
	'fields' => array(
		array(
			'key' => 'field_5ab289cb37e19',
			'label' => 'Pick a Library',
			'name' => 'library',
			'type' => 'relationship',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array(
				0 => 'slices',
			),
			'taxonomy' => array(
			),
			'filters' => array(
				0 => 'search',
			),
			'elements' => '',
			'min' => '',
			'max' => '',
			'return_format' => 'id',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => false,
	'description' => '',
));

endif;
